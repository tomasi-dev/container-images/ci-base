# syntax=docker/dockerfile:1.3-labs
FROM docker.io/library/debian:stable-slim

ARG TARGETOS
ARG TARGETARCH

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# hadolint ignore=DL3008,SC2086
RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

### Install network tools
# hadolint ignore=DL3009,DL3008
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get --fix-broken --no-install-recommends install -y \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    gnupg \
    tzdata \
    lsb-release \
    bash \
    git

RUN curl -fsSL https://github.com/cashapp/hermit/releases/download/stable/install.sh | /bin/bash

COPY ./scripts/entrypoint.sh /home/entrypoint.sh

ENTRYPOINT ["scripts/entrypoint.sh"]
