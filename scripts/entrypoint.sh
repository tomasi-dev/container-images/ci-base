#!/usr/bin/env bash

if [[ -f "bin/hermit" ]]; then
  . ./bin/activate-hermit
fi

exec "$@"
